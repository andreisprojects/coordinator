//
//  MainVC.swift
//  coordinator
//
//  Created by Andrei on 4/17/18.
//  Copyright © 2018 Andrei. All rights reserved.
//

import UIKit

class MainVC: UIViewController {

    weak var coordinator: MainCoordinator?
    
    let button: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        button.setTitle("Go to Secondary VC", for: .normal)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI() {
        view.backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
        view.addSubview(button)
        
        button.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        button.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        button.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)

    }
    
    @objc private func buttonTapped() {
        coordinator?.goToVC(selectedVC: .secondary, animated: true)
//        print("tapped")
    }


}
