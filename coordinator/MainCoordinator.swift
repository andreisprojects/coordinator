//
//  MainCoordinator.swift
//  coordinator
//
//  Created by Andrei on 4/17/18.
//  Copyright © 2018 Andrei. All rights reserved.
//

import UIKit

class MainCoordinator: Coordinator {
    
    //    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    
    func goToVC(selectedVC: VCs, animated: Bool) {
        
        let mainVC = MainVC()
        let secVC = SecondaryVC()
        mainVC.coordinator = self
        secVC.coordinator = self
        
        var destinationVC = UIViewController()
        
        switch selectedVC {
        case .main:
            destinationVC = mainVC
        case .secondary:
            destinationVC = secVC
        }
        
        navigationController.pushViewController(destinationVC, animated: animated)
        //you can set animated to false to present the main VC without animation
    }
    
    
}


