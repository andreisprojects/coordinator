//
//  Coordinator.swift
//  coordinator
//
//  Created by Andrei on 4/17/18.
//  Copyright © 2018 Andrei. All rights reserved.
//

import UIKit

enum VCs {
    case main
    case secondary
}

protocol Coordinator {
//    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    func goToVC(selectedVC: VCs, animated: Bool)
}


